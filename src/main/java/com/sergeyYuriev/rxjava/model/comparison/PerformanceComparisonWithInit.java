package com.sergeyYuriev.rxjava.model.comparison;

import com.sergeyYuriev.rxjava.model.exceptions.IllegalThreadCountException;
import com.sergeyYuriev.rxjava.model.exceptions.UnsupportedSchedulerException;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.schedulers.Schedulers;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class PerformanceComparisonWithInit {

    private static final String emittingValue = "Some value";
    private ExecutorService executor;
    private Observable observable;

    public static void main(String[] args) {
        try {
            PerformanceComparisonWithInit comparison = new PerformanceComparisonWithInit();
//            printExecutionTime(executionTime(() -> comparison.doNativeConcurrency()));
//            printExecutionTime(executionTime(() -> comparison.doRxJava()));
            comparison.doNativeConcurrency();
            comparison.doRxJava();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public PerformanceComparisonWithInit() throws UnsupportedSchedulerException, IllegalThreadCountException {
        observable = Observable.just(emittingValue)
                .subscribeOn(Schedulers.single());
        executor = Executors.newSingleThreadExecutor();
    }

    private static void printExecutionTime(long time) throws Exception {
        System.out.format(" -> Finished in: %d ms\n", time);
    }

    private void doNativeConcurrency() {
        try {
            Future<String> value = executor.submit(() -> emittingValue);
            System.out.printf("Do native concurrency: %s", value.get());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            executor.shutdown();
        }
    }

    private void doRxJava() {
        observable.blockingSubscribe(value -> System.out.printf("Do RxJava concurrency: %s", value),
                throwable -> ((Throwable) throwable).printStackTrace());
    }

}
