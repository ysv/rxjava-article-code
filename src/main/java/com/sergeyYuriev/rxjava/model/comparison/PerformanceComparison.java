package com.sergeyYuriev.rxjava.model.comparison;

import com.sergeyYuriev.rxjava.model.exceptions.IllegalThreadCountException;
import com.sergeyYuriev.rxjava.model.exceptions.UnsupportedSchedulerException;
import io.reactivex.rxjava3.core.Observable;

import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class PerformanceComparison {

    private static final String emittingValue = "Some value";
    private ExecutorService executor;

    public static void main(String[] args) {
        PerformanceComparison comparison = null;
        try {
            comparison = new PerformanceComparison();
//            printExecutionTime(executionTime(comparison::doNativeConcurrency));
//            printExecutionTime(executionTime(comparison::doRxJava));
            comparison.doNativeConcurrency();
            comparison.doRxJava();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (Objects.nonNull(comparison.executor)) {
                comparison.executor.shutdown();
            }
        }
    }

    public PerformanceComparison() throws UnsupportedSchedulerException, IllegalThreadCountException {
        executor = Executors.newSingleThreadExecutor();
    }

    private static void printExecutionTime(long time) throws Exception {
        System.out.format(" -> Finished in: %d ms\n", time);
    }

    private void doNativeConcurrency() {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        try {
            Future<String> value = executor.submit(() -> emittingValue);
            System.out.printf("Do native concurrency: %s\n", value.get());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            executor.shutdown();
        }
    }

    private void doRxJava() {
//        Observable.just(emittingValue)
        Observable.create(emitter -> {
            executor.execute(() -> {
                try {
                    emitter.onNext(emittingValue);
                    emitter.onComplete();
                } catch (Exception e) {
                    emitter.onError(e);
                }
            });
        })
//        .subscribeOn(Schedulers.single())
        .blockingSubscribe(value -> System.out.printf("Do RxJava concurrency: %s", value), Throwable::printStackTrace);
    }

}
