package com.sergeyYuriev.rxjava.model.backgroundWork;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.schedulers.Schedulers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.sergeyYuriev.rxjava.model.utils.Execution.currentThreadName;

public class BackgroundWork {

    // Повторить пример с загрузкой url
    public static void main(String[] args) {
        showAfterSave();
    }

    private static void showAfterSave() {
        // Загружаем данные по сети используя планировщик IO
        getProductsFromNetwork()
            .subscribeOn(Schedulers.io())
            // Выбираем только продукты со скидкой
            // используя планировщик COMPUTATION
            .observeOn(Schedulers.computation())
            .filter(product -> {
                System.out.printf("Filtering %d product image on %s\n", product.num, currentThreadName());
                return product.discount;
            })
            // Добавляем надпись "Скидка" на изображение продукта
            .flatMap(BackgroundWork::processProductImages)
            // Сохраняем изображения всех продуктов в базу данных используя поланировщик IO
            .observeOn(Schedulers.io())
            .map(image -> {
                saveImageToDB(image);
                return image;
            })
            // Берем изображения трех последних продуктов
            .scan(new ArrayList<ProductImage>(), (list, item) -> {
                list.add(item);
                return new ArrayList<>(list.subList(Math.max(list.size() - 3, 0), list.size()));
            })
            // Показываем их на экране в главном потоке main UI thread
//            .observeOn(AndroidSchedulers.mainThread())
//            .subscribe(productImages -> updateUI(productImages), throwable -> log(throwable));
            .blockingSubscribe(BackgroundWork::updateUI, BackgroundWork::proceedError);
    }

    private static Observable<Product> getProductsFromNetwork() {
        return Observable.timer(100, TimeUnit.MILLISECONDS)
                .create(emitter -> {
                    try {
                        for (int i = 0; i < 5; i++) {
                            System.out.printf("Loading %d product from network on %s\n", i, currentThreadName());
                            emitter.onNext(new Product(i, true));
                        }
                        emitter.onComplete();
                    } catch (Exception e) {
                        emitter.onError(e);
                    }

                });
    }

    private static Observable<ProductImage> processProductImages(Product product) {
        return Observable.timer(5000, TimeUnit.MILLISECONDS)
                .create(emitter -> {
                    System.out.printf("Processing %d product images on %s\n", product.num, currentThreadName());
                    try {
                        emitter.onNext(new ProductImage(product.num));
                        emitter.onComplete();
                    } catch (Exception e) {
                        emitter.onError(e);
                    }
                });
    }


    private static void updateUI(List<ProductImage> images) {
        // Update UI logic
        String imagesNames = getImagesNames(images);
        System.out.printf("Updating UI on %s\n%s\n", currentThreadName(), imagesNames);
    }

    private static String getImagesNames(List<ProductImage> images) {
        StringBuilder imagesNames = new StringBuilder("[");
        Iterator<ProductImage> iter = images.iterator();
        while (iter.hasNext()) {
            imagesNames.append(String.format("image %d", iter.next().num));
            if (iter.hasNext()) {
                imagesNames.append(",");
            }
        }
        imagesNames.append("]");

        return imagesNames.toString();
    }

    private static void doOnComplete() {
        // some logic
    }

    private static void proceedError(Throwable throwable) {
        System.out.println(throwable);
        // Log logic
    }

    private static void saveImageToDB(ProductImage image) {
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.printf("Saving %d image to DB on %s\n", image.num, currentThreadName());
    }

    private static class Product {

        public int num;
        public boolean discount = true;

        public Product(int num, boolean discount) {
            this.num = num;
            this.discount = discount;
        }

    }

    private static class ProductImage {

        public int num;

        public ProductImage(int num) {
            this.num = num;
        }

    }

}
