package com.sergeyYuriev.rxjava.model.logParsing;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.schedulers.Schedulers;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.concurrent.Executors;

import static com.sergeyYuriev.rxjava.model.utils.Execution.currentThreadName;

public class RxLogParsing {

    private static final String INPUT_LOG = "logs.txt";

    private static class ParseResult {
    }

    public static void main(String[] args) {
        Observable.<String>create(emitter -> {
            // Построчно читаем весь файл
            BufferedReader reader = new BufferedReader(new FileReader(INPUT_LOG));
            try {
                String line;
                while ((line = reader.readLine()) != null) {
                    emitter.onNext(line);
                }

                emitter.onComplete();
            } catch (Exception e) {
                emitter.onError(e);
            }
        })
        // Группируем строки по 1000 штук
        .buffer(1000)
        // Используе для обработки планировщик Computation
        .observeOn(Schedulers.computation())
        // Паралельно обрабатываем группы строк
        .flatMap(list -> Observable.fromIterable(list)
                            .map(line -> parseLog(line)))
        // Дальше выполняем работу на собственном планировщике,
        // который создаем из пула с тремя потоками исполнения
        .observeOn(Schedulers.from(Executors.newFixedThreadPool(3)))
        // Параллельно сохраняем результаты парсинга
        // в базу данных
        .flatMap(parseResult -> Observable.just(parseResult)
                                    .map(result -> {
                                        saveToDB(result);
                                        return "OK";
                                    }))
        // подсчитываем количество сохраненных результатов
        .reduce(0, (totalCount, line) -> ++totalCount)
        // Осуществляем подписку
        .blockingSubscribe(totalCount -> System.out.printf("Found %d valid lines\n", totalCount)
                , Throwable::printStackTrace);
    }

    private static ParseResult parseLog(String line) {
        // Логика парсинга
        System.out.printf("Parsing %s line on %s\n", line, currentThreadName());
        return new ParseResult();
    }

    private static void saveToDB(ParseResult parseResult) {

    }

}
