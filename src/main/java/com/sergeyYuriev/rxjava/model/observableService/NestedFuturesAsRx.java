package com.sergeyYuriev.rxjava.model.observableService;

import com.sergeyYuriev.rxjava.model.utils.SchedulerFactory;
import com.sergeyYuriev.rxjava.model.utils.SchedulerFactory.SchedulerQuery;
import com.sergeyYuriev.rxjava.model.utils.SchedulerFactory.SchedulerType;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Scheduler;
import io.reactivex.rxjava3.schedulers.Schedulers;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

public class NestedFuturesAsRx {

    private static final int corePoolSize = Runtime.getRuntime().availableProcessors();

    private static Scheduler scheduler = null;

    public static void main(String[] args) {
        try {
            scheduler = SchedulerFactory.getScheduler(
                    new SchedulerQuery().setSchedulerType(SchedulerType.COMPUTATION).setThreadCount(corePoolSize));

//            System.out.format("Finished in: %d ms", Execution.executionTime(() -> run(scheduler)));
            NestedFuturesAsRx.run(scheduler);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (Objects.nonNull(scheduler)) {
                scheduler.shutdown();
            }
        }
    }

    private static void run(Scheduler scheduler) {
//        System.out.printf(Locale.ENGLISH, "\nBefore subscription: %s\n", currentThreadName());
        // Получаем значение из callToRemoteServiceA и затем
        // используем его для вызова callToRemoteServiceC
        Observable<String> aStream = callToRemoteServiceASleep(scheduler)
                .flatMap(value -> callToRemoteServiceCSleep(value, scheduler));
        // Получаем значение из callToRemoteServiceB и затем
        // используем его для вызова callToRemoteServiceD и callToRemoteServiceE.
        // После этого перемножаем результаты от callToRemoteServiceD и callToRemoteServiceE
        Observable<Integer> bStream = callToRemoteServiceBSleep(scheduler)
                .flatMap(value -> Observable
                .zip(callToRemoteServiceDSleep(value, scheduler), callToRemoteServiceESleep(value, scheduler), (d, e) -> d * e));
        // Используем значения из двух предыдущих потоков для полчения
        // окончательного результата
        Observable.zip(aStream, bStream, (a, b) -> String.format("%s => %d", a, b))
                .subscribeOn(scheduler)
                .blockingSubscribe(System.out::println, Throwable::printStackTrace);
        // Важно отметить, что к потоку применяется планировщик, из первого оператора subscribeOn,
        // стоящего после создания потока.
    }

    private static Observable<String> callToRemoteServiceA(Scheduler scheduler) {
        return Observable.timer(100, TimeUnit.MILLISECONDS).map(delay -> "responseA").subscribeOn(scheduler);
    }

    private static Observable<String> callToRemoteServiceASleep(Scheduler scheduler) {
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return Observable.just("responseA").subscribeOn(scheduler);
    }

    private static Observable<Integer> callToRemoteServiceB(Scheduler scheduler) {
        return Observable.timer(40, TimeUnit.MILLISECONDS).map(delay -> 100).subscribeOn(Schedulers.computation());
    }

    private static Observable<Integer> callToRemoteServiceBSleep(Scheduler scheduler) {
        try {
            Thread.sleep(40);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return Observable.just(100).subscribeOn(scheduler);
    }

    private static Observable<String> callToRemoteServiceC(String dependencyFromA, Scheduler scheduler) {
        return Observable.timer(60, TimeUnit.MILLISECONDS).map(delay -> "responseB_" + dependencyFromA)
                .subscribeOn(scheduler);
    }

    private static Observable<String> callToRemoteServiceCSleep(String dependencyFromA, Scheduler scheduler) {
        try {
            Thread.sleep(60);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return Observable.just("responseB_" + dependencyFromA).subscribeOn(scheduler);
    }

    private static Observable<Integer> callToRemoteServiceD(int dependencyFromB, Scheduler scheduler) {
        return Observable.timer(140, TimeUnit.MILLISECONDS).map(delay -> 40 + dependencyFromB).subscribeOn(scheduler);
    }

    private static Observable<Integer> callToRemoteServiceDSleep(int dependencyFromB, Scheduler scheduler) {
        try {
            Thread.sleep(140);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return Observable.just(40 + dependencyFromB).subscribeOn(scheduler);
    }

    private static Observable<Integer> callToRemoteServiceE(int dependencyFromB, Scheduler scheduler) {
        return Observable.timer(55, TimeUnit.MILLISECONDS).map(delay -> 5000 + dependencyFromB).subscribeOn(scheduler);
    }

    private static Observable<Integer> callToRemoteServiceESleep(int dependencyFromB, Scheduler scheduler) {
        try {
            Thread.sleep(55);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return Observable.just(5000 + dependencyFromB).subscribeOn(scheduler);
    }

}
