package com.sergeyYuriev.rxjava.model.utils;

import java.util.concurrent.Executors;

import com.sergeyYuriev.rxjava.model.exceptions.IllegalThreadCountException;
import com.sergeyYuriev.rxjava.model.exceptions.UnsupportedSchedulerException;

import io.reactivex.rxjava3.core.Scheduler;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class SchedulerFactory {

    public enum SchedulerType {
        SINGLE, COMPUTATION, CUSTOM
    }

    public static class SchedulerQuery {

        private SchedulerType schedulerType;
        private int threadCount;

        public SchedulerType getSchedulerType() {
            return schedulerType;
        }

        public SchedulerQuery setSchedulerType(SchedulerType schedulerType) {
            this.schedulerType = schedulerType;

            return this;
        }

        public int getThreadCount() {
            return threadCount;
        }

        public SchedulerQuery setThreadCount(int threadCount) {
            this.threadCount = threadCount;

            return this;
        }

    }

    public static Scheduler getScheduler(SchedulerQuery query)
            throws UnsupportedSchedulerException, IllegalThreadCountException {
        switch (query.getSchedulerType()) {
            case SINGLE:
                return Schedulers.single();
            case COMPUTATION:
                return Schedulers.computation();
            case CUSTOM:
                return getCustomScheduler(query.threadCount);
            default:
                String message = String.format("Unsupported schedule type: %s", query.getSchedulerType());
                throw new UnsupportedSchedulerException(message);
        }
    }

    private static Scheduler getCustomScheduler(int threadCount) throws IllegalThreadCountException {
        if (threadCount <= 0) {
            String message = String.format("Illegal thread count: %d", threadCount);
            throw new IllegalThreadCountException(message);
        }

        return Schedulers.from(Executors.newFixedThreadPool(threadCount));
    }

}
