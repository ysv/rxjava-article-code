package com.sergeyYuriev.rxjava.model.utils;

import com.google.common.base.Stopwatch;

import java.util.concurrent.TimeUnit;

public class Execution {

    public interface VoidFunction {
        void apply() throws Exception;
    }
    
    public static long executionTime(VoidFunction function) throws Exception {
        Stopwatch stopwatch = Stopwatch.createStarted();
        function.apply();
        stopwatch.stop();

        return stopwatch.elapsed(TimeUnit.MILLISECONDS);
    }
    
    public static String currentThreadName() {
        return Thread.currentThread().toString();
    }

}
