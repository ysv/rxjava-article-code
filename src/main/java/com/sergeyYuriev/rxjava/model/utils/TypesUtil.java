package com.sergeyYuriev.rxjava.model.utils;

public class TypesUtil {

    public static class Pair<F, S> {
        public F first;
        public S second;

        public Pair(F first, S second) {
            this.first = first;
            this.second = second;
        }
    }

}
