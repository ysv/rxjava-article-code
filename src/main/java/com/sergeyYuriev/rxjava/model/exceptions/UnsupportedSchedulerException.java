package com.sergeyYuriev.rxjava.model.exceptions;

public class UnsupportedSchedulerException extends BaseException {

    private static final long serialVersionUID = 815267434918428230L;

    public UnsupportedSchedulerException(String message) {
        super(message);
    }

}
