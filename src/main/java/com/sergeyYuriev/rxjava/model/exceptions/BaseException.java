package com.sergeyYuriev.rxjava.model.exceptions;

public class BaseException extends Exception {
    
    private static final long serialVersionUID = -1871741793680951698L;

    public BaseException(String message) {
        super(message);
    }
    
    public BaseException(String message, Throwable throwable) {
        super(message, throwable);
    }

}
