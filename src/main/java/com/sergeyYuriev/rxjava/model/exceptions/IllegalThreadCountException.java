package com.sergeyYuriev.rxjava.model.exceptions;

public class IllegalThreadCountException extends BaseException {

    private static final long serialVersionUID = 5567595762626172798L;

    public IllegalThreadCountException(String message) {
        super(message);
    }

}
